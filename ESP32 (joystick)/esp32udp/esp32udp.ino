#include <WiFi.h>
#include <WiFiUdp.h>

/* WiFi network name and password */
const char * ssid = "Smart_lab";
const char * pwd = "Smart_lab";

// IP address to send UDP data to.
// it can be ip address of the server or 
// a network broadcast address
// here is broadcast address
const char * udpAddress = "86.50.253.157";
const int udpPort = 30030;

const int Id = 1234;

// These constants won't change. They're used to give names to the pins used:
const int analogInPinX = 34;  // Analog input pin that the potentiometer is attached to
const int analogInPinY = 35;  // Analog input pin that the potentiometer is attached to

String createPacket(int dataX, int dataY);

//create UDP instance
WiFiUDP udp;

void setup(){
  Serial.begin(115200);
  
  //Connect to the WiFi network
   WiFi.begin(ssid, pwd);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  //This initializes udp and transfer buffer
  udp.begin(udpPort);
}

void loop(){
  //data will be sent to server
  uint8_t buffer[50] = "h";
  char testArr[50];
  
  //send hello world to server
  
  // read the analog in value:
  int sensorValueX = analogRead(analogInPinX);
  int sensorValueY = analogRead(analogInPinY);

  int dataX = map(sensorValueX, 0, 4095, 0, 1023);
  int dataY = map(sensorValueY, 0, 4095, 0, 1023);

  // dataX += 50;
  // dataY += 50;

  if(dataX>445 && dataX < 530) dataX = 512;
  if(dataY>445 && dataY < 530) dataY = 512;

   if(dataX >1023) dataX = 1023;
   if(dataY >1023) dataY = 1023;


  // map it to the range of the analog out:
  
  buffer[0] = 'd';
  String packet = createPacket(dataX, dataY);
  int len = packet.length();

  packet.toCharArray(testArr, len+1);

  for(int i=0;i<len;i++){
    buffer[i]=testArr[i];
  }

    
 // packet.toCharArray(buffer, len);
  Serial.println(testArr);
  udp.beginPacket(udpAddress, udpPort);
  udp.write(buffer, len);
  udp.endPacket();
  memset(buffer, 0, 50);
  //processing incoming packet, must be called before reading the buffer

  //Wait for 1 second
  delay(20);
}



String createPacket(int dataX, int dataY){
  int x = dataX;
  int y = dataY;

  String a = "d,";
  a += Id;
  a += ",";
  a += x;
  a += ",";
  a += y;
  return a;


  }
