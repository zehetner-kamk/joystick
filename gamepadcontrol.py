import time
import time as t
import random
import socket
import pygame
from pygame import *
from pygame import joystick


pygame.init()
pygame.joystick.init()


id1 = random.randint(0, 1000000)  # ids of joysticks
id2 = random.randint(0, 1000000)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)

while(True):
    try:
        joy = 0  # joysticks have to be reset every time, otherwise reading doesn't work
        # causes slow read rate, but can't be avoided
        # these settings work on a ps4 controller. other controllers may have other layouts.
        joy = pygame.joystick.Joystick(0)
        x1 = joy.get_axis(0)
        y1 = joy.get_axis(1)
        x2 = joy.get_axis(3)
        y2 = joy.get_axis(4)

        current_time = str(int(t.time()*1000))

        sock.sendto(("n,"+str(id1)+","+str(x1)+","+str(y1)+","+current_time).encode('utf-8'), ("86.50.253.157", 30030))  # set server IP here
        sock.sendto(("n,"+str(id2)+","+str(x2)+","+str(y2)+","+current_time).encode('utf-8'), ("86.50.253.157", 30030))
    except:  # controller disconnected
        t.sleep(1.0)

    # t.sleep(0.016)  # 60 inputs/s, not needed since joy=0 makes it too slow anyway


s.close()
