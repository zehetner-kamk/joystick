sudo apt update
sudo apt install git --yes
sudo apt install openjdk-11-jre-headless --yes
sudo apt install nodejs --yes
sudo apt install python3 --yes
sudo apt install python3-pip --yes
pip3 install urllib3

python3 JFrontendServer/configure_ip.py


#startServer script without user input
git pull

sudo pkill "node"
sudo pkill "java"
echo "Killing old processes ..."
sleep 0.1 #wait until everything is killed

#start processes in background
java -jar JBackendServer/out/artifacts/Server.jar &
node JFrontendServer/server &
