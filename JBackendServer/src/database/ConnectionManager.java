package database;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ConnectionManager {
    private static final String CONFIG_LOCATION = "/home/ubuntu/config/joystick/";
    private static final String URL = readConfig(CONFIG_LOCATION + "url");
    private static final String DB_NAME = readConfig(CONFIG_LOCATION + "dbname");
    private static final String USERNAME = readConfig(CONFIG_LOCATION + "username");
    private static final String PASSWORD = readConfig(CONFIG_LOCATION + "password");
    private static InfluxDB influxDB = null;


    public static InfluxDB getConnection() {
        if (influxDB == null) {
            influxDB = InfluxDBFactory.connect(URL, USERNAME, PASSWORD);
            influxDB.setDatabase(DB_NAME);
        }
        return influxDB;
    }

    private static String readConfig(String filename) {
        try {
            Scanner s = new Scanner(new File(filename));
            return s.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
