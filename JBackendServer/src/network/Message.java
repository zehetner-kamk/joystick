package network;

public class Message {
    private MsgT type;
    private String content;
    private long time;
    private String sourceIP;
    private Integer sourcePort;
    private String destinationIP;
    private Integer destinationPort;

    public enum MsgT {d, n, e} //data (deprecated), data, error

    public Message(MsgT type, String content, String sourceIP, Integer sourcePort, String destinationIP, Integer destinationPort) {
        this.type = type;
        this.content = content;
        this.time = System.currentTimeMillis();
        this.sourceIP = sourceIP;
        this.sourcePort = sourcePort;
        this.destinationIP = destinationIP;
        this.destinationPort = destinationPort;
    }

    public Message(Message m) {
        this.content = m.getContent();
        this.sourceIP = m.getSourceIP();
        this.sourcePort = Integer.valueOf(m.getSourcePort());
        this.destinationIP = m.getDestinationIP();
        this.destinationPort = Integer.valueOf(m.getDestinationPort());
    }

    public MsgT getType() {
        return type;
    }

    public void setType(MsgT type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getSourceIP() {
        return sourceIP;
    }

    public Integer getSourcePort() {
        return sourcePort;
    }

    public String getDestinationIP() {
        return destinationIP;
    }

    public Integer getDestinationPort() {
        return destinationPort;
    }

}
