package logic.impl;

import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPSender;
import network.Viewer;

import java.util.LinkedList;

public class JoystickHandler extends MessageHandlerAbstract {
    private int joyID;
    private String ip;
    private int port;
    private LinkedList<Message> dataQ = new LinkedList<>();
    private double latestX = 50; //percent values,0,0 is upper left corner
    private double latestY = 50;
    private long lastInputTime;
    private Viewer viewer;
    private boolean active = true; //active connection

    public JoystickHandler(int joyID, String ip, int port, Viewer viewer) {
        this.joyID = joyID;
        this.ip = ip;
        this.port = port;
        this.viewer = viewer;
        new Thread(this::pollMessage).start();
    }


    private void pollMessage() { //format: id, speedX, speedY
        while (active) {
            Message m = dataQ.pollFirst();
            if (m != null) {
                String[] content = m.getContent().split(",");
                if (content.length >= 3) {
                    if (parseInteger(content[0], m.getType(), ip, port) == joyID) {
                        double speedX = parseDouble(content[1], m.getType(), ip, port); //both between 0 and 1023
                        double speedY = parseDouble(content[2], m.getType(), ip, port);

                        if (m.getType() == Message.MsgT.d) { //d == old, deprecated format
                            speedX = convertDeprecatedSpeed(speedX);
                            speedY = convertDeprecatedSpeed(speedY);
                        } else { //new data format contains timestamp in message
                            long time = parseLong(content[3], Message.MsgT.n, ip, port);
                            if (time != -2) m.setTime(time);
                        }

                        if (speedX >= -1 && speedY >= -1 && speedX <= 1 && speedY <= 1) { //validate input values
                            latestX += calculateDelta(speedX, m.getTime());
                            latestY += calculateDelta(speedY, m.getTime());
                            //ensure that values stay between 0 and 100%
                            latestX = Math.max(0, latestX);
                            latestX = Math.min(100, latestX);
                            latestY = Math.max(0, latestY);
                            latestY = Math.min(100, latestY);

                            //TODO: Save to Database

                            //send data to nodeJS server
                            UDPSender.send(new Message(Message.MsgT.d, joyID + "," + latestX + "," + latestY, null, null, viewer.getIP(), viewer.getPort()));
                        } else {
                            UDPSender.send(new Message(Message.MsgT.d, "Invalid joystick values.", null, null, ip, port));
                        }
                        lastInputTime = m.getTime(); //update newest time
                    }
                } else {
                    UDPSender.send(new Message(Message.MsgT.d, "Invalid message format.", null, null, ip, port));
                }
            }
            try {
                Thread.sleep(5); //max 200 r/s
            } catch (InterruptedException e) {
            }
        }
    }

    private double calculateDelta(double speed, long newTime) {
        if (speed > -0.1 && speed < 0.1) return 0.0; //bottom 10% are dead zone to prevent drift
        if (speed < -0.95) speed = -1.0; //top 5% get rounded to full speed
        if (speed > 0.95) speed = 1.0;

        long timeDifference = newTime - lastInputTime;
        if (timeDifference < 0)
            return 0; //if request 1 travels 50ms and request 2 is sent 10ms later but travels only 20ms, r2 arrives before r1. after processing r2, r1 should be ignored

        if (timeDifference > 1000) timeDifference = 1000; //max one second
        double delta = speed * timeDifference; //amount moved
        delta /= 50; //it should take 5000ms to travel the entire distance with speed 1. Coordinates are scaled from 0-100, so delta needs to be divided by 50

        return delta;
    }

    private double convertDeprecatedSpeed(double x) { //some joysticks send data from 0 to 1023, this needs to be converted
        return x / 512 - 1;//get values to between -1 and 1
    }

    public void close() {
        this.active = false;
    }

    public LinkedList<Message> getDataQ() {
        return dataQ;
    }

    public long getLastInputTime() {
        return this.lastInputTime;
    }
}