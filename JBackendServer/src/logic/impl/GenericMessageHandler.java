package logic.impl;

import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;
import network.UDPReceiver;
import network.UDPSender;
import network.Viewer;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class GenericMessageHandler extends MessageHandlerAbstract implements MessageHandler {
    private final int TIMEOUT_MINS = 1; //joysticks get timeouted after being inactive
    private LinkedList<Message> genericMsgQ = new LinkedList<>();
    private Map<Integer, JoystickHandler> joystickHandlers = new HashMap<>();

    private Viewer viewer;

    public GenericMessageHandler() {
        UDPReceiver.setMessageHandler(this);
        new Thread(this::handleMessage).start();
        new Thread(this::timeoutHandlers).start();
    }


    public void enqueueMessage(Message m) { //all incoming messages arrive here and get queued
        //this is optimised to handle ~4800 requests per second with less than 50 backlog
        //this means that with 80 connected joysticks sending 60r/s each the queue lag is mostly <=10ms
        //real life performance may be slightly lower
        if (m == null) return;
        synchronized (genericMsgQ) {
            genericMsgQ.add(m);
        }
    }

    private void handleMessage() { //creates and destroys message handlers for every connected joystick
        while (true) {
            Message m;
            synchronized (genericMsgQ) {
                m = genericMsgQ.pollFirst();
            }

            if (m != null) {
                int joyID = parseInteger(m.getContent().split(",")[0], Message.MsgT.d, m.getSourceIP(), m.getSourcePort());

                switch (m.getType()) {
                    case d: //data from joystick, formatted 0 to 1023
                    case n: //data from joystick, formatted -1 to 1, also contains client time
                        if (joyID != -2) {
                            if (!joystickHandlers.containsKey(joyID)) {
                                joystickHandlers.put(joyID, new JoystickHandler(joyID, m.getSourceIP(), m.getSourcePort(), viewer));
                            }

                            joystickHandlers.get(joyID).getDataQ().add(m); //add message to joystick-specific queue
                        }
                        break;
                    default:
                        UDPSender.send(new Message(Message.MsgT.e, "Invalid request.", null, null, m.getSourceIP(), m.getSourcePort()));
                }
            }
        }
    }

    private void timeoutHandlers() { //joysticks get timeouted after being inactive
        while (true) {
            synchronized (joystickHandlers) {
                List<Integer> keys = new ArrayList<>(joystickHandlers.keySet());

                for (int i = 0; i < keys.size(); ++i) {
                    if (System.currentTimeMillis() - joystickHandlers.get(keys.get(i)).getLastInputTime() > TimeUnit.MINUTES.toMillis(TIMEOUT_MINS)) {
                        joystickHandlers.get(keys.get(i)).close(); //closes thread and removes it after client was inactive
                        joystickHandlers.remove(keys.get(i));
                    }
                }
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public Viewer getViewer() {
        return viewer;
    }

    public void setViewer(Viewer viewer) {
        this.viewer = viewer;
    }
}
