package logic;


import network.Message;
import network.UDPSender;

public abstract class MessageHandlerAbstract {

    protected int parseInteger(String s, Message.MsgT type, String ip, int port) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Integer " + s + " contains invalid characters.", null, null, ip, port));
            return -2;
        }
    }

    protected double parseDouble(String s, Message.MsgT type, String ip, int port) {
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Double " + s + " contains invalid characters.", null, null, ip, port));
            return -2.0;
        }
    }

    protected long parseLong(String s, Message.MsgT type, String ip, int port) {
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException e) {
            UDPSender.send(new Message(Message.MsgT.e, type.toString() + ",Long " + s + " contains invalid characters.", null, null, ip, port));
            return -2;
        }
    }


}
