import logic.impl.GenericMessageHandler;
import network.UDPReceiver;
import network.Viewer;

public class JoystickServer {
    public static void main(String[] args) {
        System.out.println("Starting Backend ...");

        GenericMessageHandler genericMessageHandler = new GenericMessageHandler();

        //viewer is in this case the nodeJS server running on the same machine
        genericMessageHandler.setViewer(new Viewer("128.214.252.140", 30031)); //UDPSender increments port by 1, so actually 30032
        UDPReceiver.start();
    }
}
