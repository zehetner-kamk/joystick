const { readFile, readFileSync } = require("fs");
const express = require("express");
const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const webSocket = require("ws");

base_dir = "/home/ubuntu/joystick/JFrontendServer/";
//base_dir = ""


//sites that the web server can handle
const app = express();
app.get("/joystick/live-view", (request, response) => {
    readFile(base_dir + "j-live-view.html", "utf8", (err, html) => {
        if (err) {
            response.status(500).send("Something's broken :(");
            console.log(err);
        }
        response.send(html);
    });
});
app.get("/joystick/documentation", (request, response) => {
    readFile(base_dir + "j-documentation.html", "utf8", (err, html) => {
        if (err) {
            response.status(500).send("Something's broken :(");
        }
        response.send(html);
    });
});
app.get("/rps/documentation", (request, response) => { //rps documentation, doesn't necessarily need to be here
    readFile(base_dir + "rps-documentation.html", "utf8", (err, html) => {
        if (err) {
            response.status(500).send("Something's broken :(");
        }
        response.send(html);
    });
});
app.listen(process.env.port || 8080, () => console.log("Starting Frontend ..."))


//udp receiver for live data transfer from Java server
server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});
server.bind(30032);
server.on('message', (msg, rinfo) => {
    console.log("sentit")
    //ws.send(`${msg}`); //sends msg with point id & coords to client
});


//websockets for live data transfer to client
const wss = new webSocket.Server({ port: 30033 });
wss.on("connection", ws => { //activates whenever new client is connected
    console.log("New client connected")


});