# This script configures the ip addresses found in some html files to the one of the computer the server is running on.
# Keep in mind that this can only be executed once, since the script looks for the phrase REPLACE in the files and overwrites them

import urllib3


# get ip address from some api
http = urllib3.PoolManager()
r = http.request("GET", "https://api.myip.com/")
ip = str(r.data).split('"')[3]

base_dir = "/home/ubuntu/joystick/JFrontendServer/"
filenames = ["j-live-view.html", "j-documentation.html"]

for name in filenames:
    file = open(base_dir + name, "r")

    lines = ""
    for line in file.readlines():
        line = line.replace("CONFIGUREIP", ip)  # replace placeholder with ip
        lines += line

    file.close()
    file = open(base_dir + name, "w")
    file.write(lines)
    file.close()
