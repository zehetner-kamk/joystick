public class UDPTester {
    public static void main(String[] args) {
        System.out.println("Starting ...");

        new Thread(() -> {
            UDPSender.controlMouse("128.214.252.140", 30030); // cPouta: 86.50.253.157 | 30000: RPSS1, 30010: RPSSM, 30020: Rubberduck, 30030: Joystick
        }).start();
        UDPReceiver.start();
    }
}
