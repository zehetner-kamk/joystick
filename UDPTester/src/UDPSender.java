import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;

import java.awt.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.Scanner;

public class UDPSender {
    private static DatagramSocket socket;
    private static final int SEND_PORT = 20000;


    public static void start(String address, int port) {
        try {
            socket = new DatagramSocket(SEND_PORT);
            while (true) {
                Scanner input = new Scanner(System.in);
                String message = input.nextLine();
                byte[] b = (message).getBytes("UTF-8");
                DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(address), port);
                socket.send(packet);
                System.out.println("SENT");
            }


        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }


    public static void attack(String address, int port) {
        try {
            socket = new DatagramSocket(SEND_PORT);
            Random r = new Random();

            int i = 0;
            int numOfJoysticks = 5;
            long before = System.currentTimeMillis();
            //System.currentTimeMillis() - before < TimeUnit.SECONDS.toMillis(30)
            while (true) {
                for (int j = 0; j < numOfJoysticks; ++j) {
                    String message = "d," + j + "," + r.nextInt(1024) + "," + r.nextInt(1024);
                    byte[] b = (message).getBytes("UTF-8");
                    DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(address), port);
                    socket.send(packet);
                    ++i;

                    //System.out.println("Sent "+ message);
                }
                if (i % 1000 == 0) System.out.println(i);


                try {
                    Thread.sleep(16); //24ms = ~40 r/s/j, 16ms = ~60 r/s/j
                } catch (InterruptedException e) {
                }
            }
            //System.out.println((i / 30) + " R/s");

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void controlMouse(String address, int port) {
        try {
            socket = new DatagramSocket(SEND_PORT);

            int i = 0;
            while (true) {
                int x = MouseInfo.getPointerInfo().getLocation().x;
                int y = MouseInfo.getPointerInfo().getLocation().y;
                x = Math.min(x, 1023);
                y = Math.min(y, 1023);

                String message = "n," + 0 + "," + x + "," + y+ ","+System.currentTimeMillis();
                byte[] b = (message).getBytes("UTF-8");
                DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(address), port);
                socket.send(packet);
                if (i % 30 == 0) System.out.println(x + " | " + y);
                ++i;


                try {
                    Thread.sleep(16); //24ms = ~40 r/s/j, 16ms = ~60 r/s/j
                } catch (InterruptedException e) {
                }
            }

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void controlController(String address, int port) {
        try {
            socket = new DatagramSocket(SEND_PORT);

            int i = 0;
            while (true) {

                /* Create an event object for the underlying plugin to populate */
                net.java.games.input.Event event = new net.java.games.input.Event();

                /* Get the available controllers */
                Controller[] controllers;
                ControllerEnvironment controllerEnvironment = ControllerEnvironment.getDefaultEnvironment();
                try {
                    controllers = controllerEnvironment.getControllers();
                    for (int j = 0; j < controllers.length; ++j) {
                        /* Remember to poll each one */
                        controllers[j].poll();

                        /* Get the controllers event queue */
                        net.java.games.input.EventQueue queue = controllers[j].getEventQueue();

                        /* For each object in the queue */
                        while (queue.getNextEvent(event)) {
                            /* Get event component */
                            Component comp = event.getComponent();/* Process event (your awesome code) */
                            System.out.println(comp);
                        }
                    }
                } catch (Exception e) {
                }

/*
                String message = "d," + 0 + "," + x + "," + y;
                byte[] b = (message).getBytes("UTF-8");
                DatagramPacket packet = new DatagramPacket(b, b.length, InetAddress.getByName(address), port);
                socket.send(packet);
                if (i % 30 == 0) System.out.println(x + " | " + y);
                ++i;
*/

                try {
                    Thread.sleep(100); //24ms = ~40 r/s/j, 16ms = ~60 r/s/j
                } catch (InterruptedException e) {
                }
            }

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}
