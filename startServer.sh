git reset --hard
git pull

echo ""
echo "This script is going to kill all currently running Java and nodeJS processes."
echo "Press Enter to continue or Ctrl-C to cancel."
read x
sudo pkill "node"
sudo pkill "java"
echo "Killing old processes ..."
sleep 0.1 #wait until everything is killed

#start processes in background
python3 JFrontendServer/configure_ip.py
java -jar JBackendServer/out/artifacts/Server.jar &
node JFrontendServer/server &
